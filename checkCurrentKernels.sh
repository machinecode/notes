#!/usr/bin/env bash
cd ~/kernels
#echo "fetching current package FILE_LIST"
#wget -O FILE_LIST https://mirrors.slackware.com/slackware/slackware64-current/slackware64/FILE_LIST
wget -O FILE_LIST https://slackware.osuosl.org/slackware64-current/slackware64/FILE_LIST
echo "Latest slackware64-current kernel:  `grep kernel-source*.txz.a* FILE_LIST | awk -F- '{ print $11 }'`"
echo "Currently installed kernel version: `uname -r`"
