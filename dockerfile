FROM tianon/steam
RUN sudo apt-get update && sudo apt-get install -yq kmod mesa-utils
USER steam
ENV HOME /home/steam
VOLUME /home/steam
CMD ["steam"]
