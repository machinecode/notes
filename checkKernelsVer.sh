#!/usr/bin/env bash
cd ~/kernels
echo "fetching 15.0 package FILELIST.TXT"
#wget -O FILE_LIST https://mirrors.slackware.com/slackware/slackware64-15.0/slackware64/FILE_LIST
#wget -O FILE_LIST https://slackware.osuosl.org/slackware64-15.0/slackware64/FILE_LIST
wget -O FILELIST.TXT http://ftp.slackware.com/pub/slackware/slackware64-15.0/FILELIST.TXT
echo "Latest slackware64-15.0 kernel:  `grep -i patches\/packages\/linux-5.*\/kernel-source FILELIST.TXT | awk -F- '{ print $12 }' | uniq`"
echo "Currently installed kernel version: `uname -r`"
