echo "routing ap0 traffic to burp on 8888 and 8443 invisible proxy"

/usr/sbin/iptables -t nat -A PREROUTING -i ap0 -p tcp --dport 80 -j REDIRECT --to-port 8888
/usr/sbin/iptables -t nat -A PREROUTING -i ap0 -p tcp --dport 443 -j REDIRECT --to-port 8443
