#!/usr/local/env bash
echo "Running Metasploit Framework as current user: $USER"
docker run --rm -it -p 443:443 -p 4444:4444 -v ~/.msf4:/root/.msf4 -v /tmp/msf:/tmp/data remnux/metasploit
#sudo docker run --rm -it -p 443:443 -v ~/.msf4:/root/.msf4 -v /tmp/msf:/tmp/data remnux/metasploit
