#!/usr/bin/env bash
### xbrightness <backlight 0-65535> <gamma 0-10>
# xbrightness 65535 1.0
### xrandr | grep " connected"
### anything over 1 increases gamma. 1 should be max brightness
xrandr --output eDP-1 --brightness 1
