#!/usr/bin/env bash
## 
echo "do not run this, instead modify values to get what you need"
echo "command:"
echo "openssl pkcs12 -export -out Cert.p12 -in cert.pem -inkey key.pem -passin pass:root -passout pass:root"
