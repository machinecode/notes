#!/usr/bin/env bash
cd ~/kernels
echo "fetching current package list"
#wget -O FILE_LIST https://mirrors.slackware.com/slackware/slackware64-current/slackware64/FILE_LIST
wget -O FILE_LIST https://slackware.osuosl.org/slackware64-current/slackware64/FILE_LIST
echo "backing up old kernels"
mv kernel*.t?z* oldKernels/
#echo "fetching (wget -m -np) kernel packages"
echo "fetching (wget kernels awk cut direct)"
#wget -m -np https://mirrors.slackware.com/slackware/slackware64-current/slackware64/k/
#for i in `grep -i kernel FILE_LIST | awk '{ print $8 }' | cut -b 2-`; do wget https://mirrors.slackware.com/slackware/slackware64-current/slackware64/$i ; done
### Using OSUOSL as it is usually "fresher"
for i in `grep -i kernel FILE_LIST | awk '{ print $8 }' | cut -b 2-`; do wget https://slackware.osuosl.org/slackware64-current/slackware64/$i ; done
echo "removing kernel*.txt files"
rm kernel*.txt
#echo "Moving kernel and cleaning up folders"
#mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/*.t?z ~/kernels/
#mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/*.asc ~/kernels/
#mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/maketag ~/kernels/
#mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/tagfile ~/kernels/
#mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/install-packages ~/kernels/
#rm -fr ~/kernels/mirrors.slackware.com
echo "Latest Kernel Mirrored to ~/kernels"

