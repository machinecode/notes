echo "for android, use this to redirect to ap for testing"
#Flush iptables NAT table

adb shell "iptables -t nat -F"

#Redirect HTTP traffic to Burp

adb shell "iptables -t nat -A OUTPUT -p tcp --dport 80 -j DNAT --to-destination 192.168.0.2:8080

#Redirect HTTPS traffic to Burp

adb shell "iptables -t nat -A OUTPUT -p tcp --dport 443 -j DNAT --to-destination 192.168.0.2:8080

#Activate Masquerading

adb shell "iptables -t nat -A POSTROUTING -p tcp --dport 80 -j MASQUERADE"
adb shell "iptables -t nat -A POSTROUTING -p tcp --dport 443 -j MASQUERADE"

