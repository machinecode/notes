#!/usr/bin/env bash
export VERSION=5.15.145
echo "Building vmlinuz and initrd for kernel version $VERSION on XFS format root"
mkinitrd -c -k $VERSION -f xfs -r /dev/cryptvg/root -m usbhid:hid_generic:uhci-hcd:xfs:nvme:vfat:ext4:i915:tpm -C /dev/nvme0n1p3 -L -u -h /dev/cryptvg/swap -o /boot/initrd.gz.$VERSION
echo "Backing up current EFI boot files, vmlinuz and initrd.gz"
if [ ! -f /boot/oldEFI/vmlinuz.pre_$VERSION ]; then
  cp -n /boot/efi/EFI/boot/vmlinuz /boot/oldEFI/vmlinuz.pre_$VERSION
else
  echo "vmlinuz.pre_$VERSION backup exists, skipping"
fi
if [ ! -f /boot/oldEFI/initrd.gz.pre_$VERSION ]; then
  cp -n /boot/efi/EFI/boot/initrd.gz /boot/oldEFI/initrd.gz.pre_$VERSION
else
  echo "initrd.gz.pre_$VERSION backup exists, skipping"
fi
echo "copying vmlinuz and initrd.gz to EFI partition"
cp /boot/vmlinuz-generic /boot/efi/EFI/boot/vmlinuz
cp /boot/initrd.gz.$VERSION /boot/efi/EFI/boot/initrd.gz
echo "Build, Backup, Copy complete. Reboot for new kernel: $VERSION"