#!/usr/bin/env bash
cd ~/kernels
echo "fetching (wget -m -np) kernel packages"
wget -m -np https://mirrors.slackware.com/slackware/slackware64-current/slackware64/k/
echo "Moving kernel"
#echo "Moving kernel and cleaning up folders"
mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/*.t?z ~/kernels/
mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/*.asc ~/kernels/
mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/maketag ~/kernels/
mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/tagfile ~/kernels/
#mv ~/kernels/mirrors.slackware.com/slackware/slackware64-current/slackware64/k/install-packages ~/kernels/
#rm -fr ~/kernels/mirrors.slackware.com
echo "Latest Kernel Mirrored to ~/kernels"
##
# todo: get all kernel packages and update, not just kernel
#kernel-firmware-20210322_ ... .txz
#kernel-generic
#kernel-headers
#kernel-huge
#kernel-modules
#kernel-source
